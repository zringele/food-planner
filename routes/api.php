<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IngredientController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\RecipeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('ingredients', IngredientController::class)->except(['edit', 'create']);
Route::resource('plans', PlanController::class)->except(['edit', 'create']);
Route::resource('recipes', RecipeController::class)->except(['edit', 'create']);

Route::get('plans/{plan}/shopping_list/{from}/{to}', [PlanController::class, 'productsNeeded']);
