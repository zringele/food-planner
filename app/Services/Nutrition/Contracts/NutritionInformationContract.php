<?php
declare(strict_types=1);

namespace App\Services\Nutrition\Contracts;


interface NutritionInformationContract
{
    public function addKcal(float $kcal): void;
    public function addProteins(float $proteins): void;
    public function addCarbs(float $carbs): void;
    public function addFat(float $fat): void;

    public function getKcal(): float;
    public function getProteins(): float;
    public function getCarbs(): float;
    public function getFat(): float;
}
