<?php
declare(strict_types=1);

namespace App\Services\Nutrition\Contracts;


use App\Models\Plan;
use App\Models\Recipe;

interface NutritionCalculatorContract
{
    public function calculateIngredients(Recipe $recipe, NutritionInformationContract $nutritionInformation = null): NutritionInformationContract;

    public function planNutrition(Plan $plan): NutritionInformationContract;

}
