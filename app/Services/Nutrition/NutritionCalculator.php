<?php


namespace App\Services\Nutrition;


use App\Models\Contracts\HasIngredients;
use App\Models\Plan;
use App\Services\Nutrition\Contracts\NutritionCalculatorContract;
use App\Services\Nutrition\Contracts\NutritionInformationContract;

class NutritionCalculator implements NutritionCalculatorContract
{

    /**
     * @param HasIngredients $recipe
     * @param NutritionInformationContract|null $nutritionInformation
     * @return NutritionInformationContract
     */
    public function calculateIngredients(HasIngredients $recipe, NutritionInformationContract $nutritionInformation = null): NutritionInformationContract
    {
        $nutritionInformation = $nutritionInformation ?? new NutritionInformation();

        $recipe->load('ingredients');

        foreach ($recipe->ingredients as $ingredient) {
            $amount = $ingredient->pivot->amount;
            $nutritionInformation->addCarbs($amount * $ingredient->carbs);
            $nutritionInformation->addFat($amount * $ingredient->fat);
            $nutritionInformation->addKcal($amount * $ingredient->kcal);
            $nutritionInformation->addProteins($amount * $ingredient->proteins);
        }

        return $nutritionInformation;
    }

    /**
     * @param Plan $plan
     * @return NutritionInformationContract
     */
    public function planNutrition(Plan $plan): NutritionInformationContract
    {
        $nutritionInformation = new NutritionInformation();
        $plan->load('recipes');

        foreach ($plan->recipes as $recipe) {
            $amount = $recipe->pivot->amount;
            $nutritionInformation->addCarbs($amount * $recipe->nutrition->carbs);
            $nutritionInformation->addFat($amount * $recipe->nutrition->fat);
            $nutritionInformation->addKcal($amount * $recipe->nutrition->kcal);
            $nutritionInformation->addProteins($amount * $recipe->nutrition->proteins);
        }
        $plan->load('ingredients');

        $this->calculateIngredients($plan, $nutritionInformation);
        return $nutritionInformation;
    }
}
