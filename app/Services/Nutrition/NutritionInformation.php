<?php
declare(strict_types=1);

namespace App\Services\Nutrition;


use App\Services\Nutrition\Contracts\NutritionInformationContract;

class NutritionInformation implements NutritionInformationContract
{
    public float $kcal = 0;
    public float $proteins = 0;
    public float $carbs = 0;
    public float $fat = 0;

    public function addKcal(float $kcal): void
    {
        $this->kcal += $kcal;
    }

    public function addProteins(float $proteins): void
    {
        $this->proteins += $proteins;
    }

    public function addCarbs(float $carbs): void
    {
        $this->carbs += $carbs;
    }

    public function addFat(float $fat): void
    {
        $this->fat += $fat;
    }

    public function getKcal(): float
    {
        return $this->kcal;
    }

    public function getProteins(): float
    {
        return $this->proteins;
    }

    public function getCarbs(): float
    {
        return $this->carbs;
    }

    public function getFat(): float
    {
        return $this->fat;
    }
}
