<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Http\Responses\JsonErrorResponse;
use App\Models\Recipe;
use App\Repositories\Contracts\RecipeRepositoryContract;
use Illuminate\Support\Facades\DB;

class RecipeRepository implements RecipeRepositoryContract
{

    public function setIngredients(Recipe $recipe, array $ingredients): Recipe
    {
        $recipe->ingredients()->detach();

        foreach ($ingredients as $ingredient) {
            $recipe->ingredients()->attach($ingredient['id'], ['amount' => $ingredient['amount'] ?? 1]);
        }

        return $recipe;
    }

    public function saveNew(array $validatedData): Recipe
    {
        DB::beginTransaction();

        try {
            $recipe = Recipe::create($validatedData);

            if (isset($validatedData['ingredients'])) {
                $this->setIngredients($recipe, $validatedData['ingredients']);
            }
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        DB::commit();

        return $recipe;
    }

    public function update(Recipe $recipe, array $validatedData): Recipe
    {

        DB::beginTransaction();

        try {
            $recipe->update($validatedData);

            if (isset($validatedData['ingredients'])) {
                $this->setIngredients($recipe, $validatedData['ingredients']);
            }
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }
        DB::commit();

        return $recipe;
    }


}
