<?php


namespace App\Repositories\Contracts;


use App\Models\Plan;
use Carbon\CarbonInterface;
use Illuminate\Support\Collection;

interface PlanRepositoryContract
{
    /**
     * @param Plan $plan
     * @param array $ingredients
     * @return Plan
     */
    public function setIngredients(Plan $plan, array $ingredients): Plan;

    public function setRecipes(Plan $plan, array $recipes): Plan;

    public function getTotalIngredients(Plan $plan, CarbonInterface $from, CarbonInterface $to): Collection;

    public function saveNew(array $validatedData): Plan;

    public function update(Plan $plan, array $validatedData): Plan;
}
