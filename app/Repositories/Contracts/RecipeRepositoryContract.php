<?php


namespace App\Repositories\Contracts;


use App\Models\Recipe;

interface RecipeRepositoryContract
{
    /**
     * @param Recipe $recipe
     * @param array $ingredients List of ingredients with id and amount
     * @return Recipe
     */
    public function setIngredients(Recipe $recipe, array $ingredients): Recipe;

    public function saveNew(array $validatedData): Recipe;

    public function update(Recipe $recipe, array $validatedData): Recipe;
}
