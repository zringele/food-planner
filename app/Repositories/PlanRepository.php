<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\Plan;
use App\Repositories\Contracts\PlanRepositoryContract;
use Carbon\CarbonInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PlanRepository implements PlanRepositoryContract
{

    public function setIngredients(Plan $plan, array $ingredients): Plan
    {
        $plan->ingredients()->detach();

        foreach ($ingredients as $ingredient) {
            $plan->ingredients()->attach($ingredient['id'], [
                'amount' => $ingredient['amount'] ?? 1,
                'timestamp' => $ingredient['timestamp'] ?? Carbon::now()->toDateTimeString()
            ]);
        }

        return $plan;
    }

    public function setRecipes(Plan $plan, array $recipes): Plan
    {
        $plan->recipes()->detach();

        foreach ($recipes as $recipe) {
            $plan->recipes()->attach($recipe['id'], [
                'amount' => $recipe['amount'] ?? 1,
                'timestamp' => $recipe['timestamp'] ?? Carbon::now()->toDateTimeString()
            ]);
        }

        return $plan;
    }

    public function getTotalIngredients(Plan $plan, CarbonInterface $from, CarbonInterface $to): Collection
    {
        // Subquery for recipe ingredient totals
        $recipeIngredientQuery = $plan->selectRaw('
            ingredients.id,
            ingredients.name,
            plan_recipe.amount * recipe_ingredient.amount * kcal as total_kcal,
            plan_recipe.amount * recipe_ingredient.amount  * fat as total_fat,
            plan_recipe.amount * recipe_ingredient.amount  * carbs as total_carbs,
            plan_recipe.amount * recipe_ingredient.amount  * proteins as total_proteins,
            plan_recipe.amount * recipe_ingredient.amount  as total_amount
            ')
            ->join('plan_recipe', 'plan_recipe.plan_id', 'plans.id')
            ->join('recipe_ingredient', 'recipe_ingredient.recipe_id', 'plan_recipe.recipe_id')
            ->join('ingredients', 'ingredients.id', 'recipe_ingredient.ingredient_id')
            ->where('plans.id', $plan->id)
            ->whereBetween('plan_recipe.timestamp', [$from->toDateTimeString(), $to->toDateTimeString()]);

        // Subquery for ingredient totals
        $ingredientQuery = $plan->selectRaw('
            ingredients.id,
            ingredients.name,
            amount * kcal as total_kcal,
            amount  * fat as total_fat,
            amount  * carbs as total_carbs,
            amount  * proteins as total_proteins,
            amount as total_amount
            ')
            ->join('plan_ingredient', 'plan_ingredient.plan_id', 'plans.id')
            ->join('ingredients', 'ingredients.id', 'plan_ingredient.ingredient_id')
            ->where('plans.id', $plan->id)
            ->whereBetween('plan_ingredient.timestamp', [$from->toDateTimeString(), $to->toDateTimeString()]);

        // Make subqueries into union to aggregate
        $unionQuery = $recipeIngredientQuery->unionAll($ingredientQuery);

        // Aggregate totals by ingredient id
        $result = $plan->fromSub($unionQuery, 'totals')
            ->groupBy('id')
            ->selectRaw('
                        id as ingredient_id,
                        max(name) as ingredient_name,
                        sum(total_kcal) as sum_kcal,
                        sum(total_fat) as sum_fat,
                        sum(total_carbs) as sum_carbs,
                        sum(total_proteins) as sum_proteins,
                        sum(total_amount) as sum_amount
                        ')
            ->get()
            ->toArray();

        return collect($result);
    }

    public function saveNew(array $validatedData): Plan
    {
        DB::beginTransaction();

        try {
            $plan = Plan::create($validatedData);

            if (isset($validatedData['ingredients'])) {
                $this->setIngredients($plan, $validatedData['ingredients']);
            }

            if (isset($validatedData['recipes'])) {
                $this->setRecipes($plan, $validatedData['recipes']);
            }
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        DB::commit();

        return $plan;
    }

    public function update(Plan $plan, array $validatedData): Plan
    {
        DB::beginTransaction();

        try {
            $plan->update($validatedData);

            if (isset($validatedData['ingredients'])) {
                $this->setIngredients($plan, $validatedData['ingredients']);
            }

            if (isset($validatedData['recipes'])) {
                $this->setRecipes($plan, $validatedData['recipes']);
            }
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        DB::commit();

        return $plan;
    }
}
