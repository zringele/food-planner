<?php
declare(strict_types = 1);

namespace App\Providers;

use App\Repositories\Contracts\PlanRepositoryContract;
use App\Repositories\Contracts\RecipeRepositoryContract;
use App\Repositories\PlanRepository;
use App\Repositories\RecipeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    const REPOSITORY_BINDINGS = [
        RecipeRepositoryContract::class => RecipeRepository::class,
        PlanRepositoryContract::class => PlanRepository::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (self::REPOSITORY_BINDINGS as $contract => $repository) {
            $this->app->bind($contract, $repository);
        }
    }
}
