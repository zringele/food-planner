<?php


namespace App\Http\Responses;


use Symfony\Component\HttpFoundation\JsonResponse;

class JsonErrorResponse extends JsonResponse
{

    public function __construct(\Throwable $exception, $message)
    {
        $data = ['message' => $message, 'error' => $exception->getMessage()];
        parent::__construct($data, 500);
    }
}
