<?php


namespace App\Http\Responses;


use Symfony\Component\HttpFoundation\JsonResponse;

class JsonDeleteSuccessResponse extends JsonResponse
{

    public function __construct($data = ['message' => 'Deleted'], int $status = 200, array $headers = [], bool $json = false)
    {
        parent::__construct($data, $status, $headers, $json);
    }
}
