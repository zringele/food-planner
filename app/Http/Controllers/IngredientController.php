<?php

namespace App\Http\Controllers;

use App\Http\Responses\JsonDeleteSuccessResponse;
use App\Http\Requests\StoreIngredientRequest;
use App\Http\Requests\UpdateIngredientRequest;
use App\Models\Ingredient;
use Exception;

class IngredientController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        return Ingredient::paginate(15);
    }


    /**
     * @param StoreIngredientRequest $request
     * @return Ingredient
     */
    public function store(StoreIngredientRequest $request): Ingredient
    {
        return Ingredient::create($request->validated());
    }

    /**
     * @param Ingredient $ingredient
     * @return Ingredient
     */
    public function show(Ingredient $ingredient): Ingredient
    {
       return $ingredient;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateIngredientRequest $request
     * @param Ingredient $ingredient
     * @return Ingredient
     */
    public function update(UpdateIngredientRequest $request, Ingredient $ingredient): Ingredient
    {
        $ingredient->update($request->validated());
        return $ingredient;
    }

    /**
     * @param Ingredient $ingredient
     * @return JsonDeleteSuccessResponse
     * @throws Exception
     */
    public function destroy(Ingredient $ingredient): JsonDeleteSuccessResponse
    {
        $ingredient->delete();
        return new JsonDeleteSuccessResponse();
    }
}
