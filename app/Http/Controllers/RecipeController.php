<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Responses\JsonDeleteSuccessResponse;
use App\Http\Requests\StoreRecipeRequest;
use App\Http\Requests\UpdateRecipeRequest;
use App\Http\Responses\JsonErrorResponse;
use App\Models\Recipe;
use App\Repositories\Contracts\RecipeRepositoryContract;
use App\Services\Nutrition\NutritionCalculator;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{

    /**
     * @var RecipeRepositoryContract
     */
    private RecipeRepositoryContract $repository;

    /**
     * @var NutritionCalculator
     */
    private NutritionCalculator $nutritionCalculator;

    public function __construct(RecipeRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return Recipe::paginate(15);
    }


    /**
     * @param StoreRecipeRequest $request
     * @return Recipe|JsonResponse
     */
    public function store(StoreRecipeRequest $request): Recipe|JsonErrorResponse
    {
        $validatedRequest = $request->validated();

        try {
            $recipe = $this->repository->saveNew($validatedRequest);
        } catch (\Throwable $exception) {
            return new JsonErrorResponse($exception, 'Failed saving recipe');
        }

        return $recipe;
    }

    /**
     * @param Recipe $recipe
     * @return Recipe
     */
    public function show(Recipe $recipe): Recipe
    {
        return $recipe;
    }


    /**
     * @param UpdateRecipeRequest $request
     * @param Recipe $recipe
     * @return Recipe|JsonResponse
     */
    public function update(UpdateRecipeRequest $request, Recipe $recipe): Recipe|JsonErrorResponse
    {
        $validatedRequest = $request->validated();
        try {
            $recipe = $this->repository->update($recipe, $validatedRequest);
        } catch (\Throwable $exception) {
            return new JsonErrorResponse($exception, 'Failed updating recipe');
        }
        return $recipe;
    }

    /**
     * @param Recipe $recipe
     * @return JsonDeleteSuccessResponse
     * @throws Exception
     */
    public function destroy(Recipe $recipe): JsonDeleteSuccessResponse
    {
        $recipe->delete();
        return new JsonDeleteSuccessResponse();
    }
}
