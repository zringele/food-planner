<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePlanRequest;
use App\Http\Requests\UpdatePlanRequest;
use App\Http\Responses\JsonDeleteSuccessResponse;
use App\Http\Responses\JsonErrorResponse;
use App\Models\Plan;
use App\Repositories\Contracts\PlanRepositoryContract;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{

    /**
     * @var PlanRepositoryContract
     */
    private PlanRepositoryContract $repository;

    public function __construct(PlanRepositoryContract $planRepository)
    {
        $this->repository = $planRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Plan::paginate(15);
    }

    /**
     * @param Plan $plan
     * @return Plan
     */
    public function show(Plan $plan): Plan
    {
        $plan->load(['ingredients', 'recipes']);
        return $plan;
    }

    /**
     * @param Plan $plan
     * @param string $from Date from when to calculate
     * @param string $to Date to when calculate products
     * @return Plan|JsonErrorResponse
     */
    public function productsNeeded(Plan $plan, string $from, string $to): Collection|JsonErrorResponse
    {
        try {
            $fromDate = Carbon::parse($from);
            $toDate = Carbon::parse($to);
        } catch (InvalidFormatException $e) {
            return new JsonErrorResponse($e, "Can not parse datetime");
        }

        return $this->repository->getTotalIngredients($plan, $fromDate, $toDate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePlanRequest $request
     * @return Plan|JsonErrorResponse
     */
    public function store(StorePlanRequest $request): Plan|JsonErrorResponse
    {
        try {
            $plan = $this->repository->saveNew($request->validated());
        } catch (\Throwable $exception) {
            return new JsonErrorResponse($exception, 'Failed saving recipe');
        }

        $plan->load(['ingredients', 'recipes']);
        return $plan;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Plan $plan
     * @return Response
     */
    public function update(UpdatePlanRequest $request, Plan $plan): Plan|JsonErrorResponse
    {
        try {
            $plan = $this->repository->update($plan, $request->validated());
        } catch (\Throwable $exception) {
            return new JsonErrorResponse($exception, 'Failed saving recipe');
        }

        $plan->load(['ingredients', 'recipes']);
        return $plan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Plan $plan
     * @return JsonDeleteSuccessResponse
     * @throws Exception
     */
    public function destroy(Plan $plan): JsonDeleteSuccessResponse
    {
        $plan->delete();
        return new JsonDeleteSuccessResponse();
    }
}
