<?php

namespace App\Http\Requests;

use App\Models\Ingredient;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateIngredientRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit_name' => [ Rule::in(Ingredient::$units)],
            'unit_weight' => ['numeric'],
            'kcal' => [ 'numeric'],
            'proteins' => [ 'numeric'],
            'carbs' => [ 'numeric'],
            'fat' => ['numeric'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'in' => 'The :attribute must be one of the following types: :values'
        ];
    }
}
