<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRecipeRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => ['array'],
            'instructions' => ['string'],
            'name' => ['string', 'max:255'],
            'ingredients' => ['array']
        ];
    }
}
