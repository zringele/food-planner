<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecipeRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => ['array'],
            'instructions' => ['required', 'string'],
            'name' => ['required', 'string', 'max:255'],
            'ingredients' => ['array']
        ];
    }
}
