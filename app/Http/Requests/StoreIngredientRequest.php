<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Models\Ingredient;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreIngredientRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'unique:ingredients', 'max:255'],
            'unit_name' => ['required', Rule::in(Ingredient::$units)],
            'unit_weight' => ['numeric'],
            'kcal' => ['required', 'numeric'],
            'proteins' => ['required', 'numeric'],
            'carbs' => ['required', 'numeric'],
            'fat' => ['required', 'numeric'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'in' => 'The :attribute must be one of the following types: :values'
        ];
    }
}
