<?php
declare(strict_types = 1);

namespace App\Models;

use App\Models\Contracts\HasIngredients;
use App\Services\Nutrition\Contracts\NutritionInformationContract;
use App\Services\Nutrition\NutritionCalculator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use XSSCleaner;

class Recipe extends Model implements HasIngredients
{
    use HasFactory;

    protected $fillable = ['name', 'instructions', 'tags'];

    protected $casts = [
        'tags' => 'array'
    ];

    protected $appends = ['nutrition'];

    /**
     * @var NutritionCalculator
     */
    protected NutritionCalculator $nutritionCalculator;

    public function ingredients(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class, 'recipe_ingredient')
            ->withPivot('amount');
    }

    public function __construct(array $attributes = [])
    {
        $this->nutritionCalculator = new NutritionCalculator();
        parent::__construct($attributes);
    }

    /**
     * Clean instructions from XSS since it will probably be used unescaped
     *
     * @param $value
     */
    public function setInstructionsAttribute($value)
    {
        $this->attributes['instructions'] = XSSCleaner::clean($value);
    }

    public function getNutritionAttribute(): NutritionInformationContract
    {
        return $this->nutritionCalculator->calculateIngredients($this);
    }

}
