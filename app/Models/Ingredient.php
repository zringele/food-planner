<?php
declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    const GRAMS = '100g';
    const UNITS = 'unit';

    public static $units = [self::GRAMS, self::UNITS];

    protected $fillable = ['name', 'unit_name', 'unit_weight', 'kcal', 'proteins', 'carbs', 'fat'];

//    protected $guarded = ['id', 'created_at', 'updated_at'];
}
