<?php
declare(strict_types = 1);

namespace App\Models;

use App\Models\Contracts\HasIngredients;
use App\Services\Nutrition\Contracts\NutritionInformationContract;
use App\Services\Nutrition\NutritionCalculator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Plan extends Model implements HasIngredients
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $appends = ['nutrition', 'ingredients_totals'];

    /**
     * @var mixed
     */
    private $nutritionCalculator;

    public function ingredients(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class, 'plan_ingredient')
            ->withPivot('amount', 'timestamp');
    }

    public function recipes(): BelongsToMany
    {
        return $this->belongsToMany(Recipe::class, 'plan_recipe')
            ->withPivot(['amount', 'timestamp']);
    }

    public function __construct(array $attributes = [])
    {
        $this->nutritionCalculator = new NutritionCalculator();
        parent::__construct($attributes);
    }

    public function getNutritionAttribute(): NutritionInformationContract
    {
        return $this->nutritionCalculator->planNutrition($this);
    }

    public function getIngredientsTotalsAttribute(): NutritionInformationContract
    {
        return $this->nutritionCalculator->planNutrition($this);
    }

}
