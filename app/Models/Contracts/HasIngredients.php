<?php


namespace App\Models\Contracts;


use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface HasIngredients
{
    public function ingredients(): BelongsToMany;
}
