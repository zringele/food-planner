## About application

Food planner helps you make a shopping list. Create ingredients and recipes, then get a shopping list for chosen time interval.

## Start

First set DB credentials in .env file
```
DB_USERNAME=anything
DB_PASSWORD=anything
```

Compose docker
```
docker-compose up
```

Run migration
```
docker exec -it food-planner_food-planner_1 /bin/bash && php artisan migrate
```

## API docs

You can find Postman collection at /docs, default port is :85, so for docs go to ```localhost:85/docs```
